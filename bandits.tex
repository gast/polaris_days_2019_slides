\input{latex_entete}

\newcommand{\pdfmovie}[4]{\href{run:#1}{\framebox{\parbox[c][#3][c]{#2}{\center
        #4}}}}

\title{Online Optimization and Bandits problems}%
\author{Nicolas Gast}

\begin{document}

\begin{frame}
  \titlepage
\end{frame}

\begin{frame}{What is online Optimization?}
  Decisions are make sequentially but you can learn from your past
  decisions.\bigskip

  \begin{align*}
    \text{Action} \leadsto \text{Observation} \leadsto \text{Action}
    \leadsto \text{Observation} \leadsto \dots 
  \end{align*}

  Examples from {\small
    \url{https://en.wikipedia.org/wiki/Online_optimization}}:
  \begin{itemize}{\tiny 
  \item K-server problem
  \item Job shop scheduling problem
  \item List update problem
  \item Bandit problem
  \item Secretary problem
  \item Search games 
  \item ~}\red{Ski rental problem}{\tiny 
  \item Linear search problem}
  \end{itemize}
\end{frame}

% \begin{frame}{Outline}
%   \tableofcontents
% \end{frame}

\begin{frame}{Online Optimization is everywhere on the Internet}
  {maximize the click-through rate} \only<1>{
    Google search\\
    \includegraphics[width=.8\linewidth]{searchSki}\\
    
    \begin{itemize}
    \item How does google decides what items they should show you? 
    \end{itemize}
  }
  \only<2->{
    From \url{http://www.lemonde.fr}:
    
    \only<2>{\includegraphics[width=.4\linewidth]{lemonde}}
    \only<3>{
      \begin{tabular}{cc}
        \mpage{.5}{\includegraphics[width=\linewidth]{lemonde_zoom}}
        &\mpage{.5}{
          \begin{itemize}
          \item How does Ligatus/Outbrain chooses a good title? 
          \end{itemize}
          }
      \end{tabular}
    }
  }
\end{frame}


\begin{frame}{These problems are instances of multi-armed bandit
    problem}
  A decision-maker chooses sequential decisions.
  \begin{itemize}
  \item At time $t$, she chooses $I_t\in\{1\dots n\}$. 
  \item This gives a (random) reward $R_{t,I_t}$. 
  \end{itemize}

  The decision-maker wants to maximize the sum of all rewards:
  $\sum_{t=1}^T R_{t,I_t}$.
\end{frame}

\begin{frame}{MAB are everywhere}

  \begin{itemize}
  \item Historically : sequential clinical trials (1933)\bigskip
    
  \item Now on the web :
    \begin{itemize}
    \item Ad placement
    \item Price experimentation 
    \item Search engines
    \end{itemize}\bigskip
    
  \item Other examples:
    \begin{itemize}
    \item Choosing the right expert (forecasting, scheduling) 
    \item Research project allocation
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}{ The mathematical formulation depends on many parameters}

  In this talk, I will mostly focus on \emph{stochastic},
  \emph{i.i.d.} bandits.
  \bigskip

  There are many variants 
  \begin{itemize}
  \item Knowledge: 
    \begin{itemize}
    \item Adversarial/robust ($R_i$ are chosen by an adversary)
    \item Stochastic or Markovian ($R_i$ are random)
    \end{itemize}
  \item Structure (\emph{e.g.}, combinatorial bandits)
  \item Observation (e.g., targeted advertising)
  \item Feedback
  \end{itemize}

\end{frame}



\begin{frame}{Table of Contents}
  \tableofcontents
\end{frame}

\section{Stochastic Bandits and Regret}

\begin{frame}{The Bernoulli multi-armed bandit}
  At each time $t$, the arm $i$ gives you a reward $1$ with
  probability $\mu_i$ and $1-\mu_i$ with probability $1-\mu_i$:
  \begin{equation*}
    \Proba{R_{t,i}=1} = \mu_i = 1-\Proba{R_{t,i}=0}. 
  \end{equation*}
  We assume that:
  \begin{itemize}
  \item The rewards are all independent.
  \item The decision maker does not know $\mu_i$. 
  \end{itemize}
  \bigskip
  
  % You would like to maximize:
  % \begin{align*}
  %   \esp{\sum_{t=1}^T R_{t,I_t}}.
  % \end{align*}
  There is a natural compromise between \textbf{exploration} and
  \textbf{exploitation}.
\end{frame}

\begin{frame}{Motivation}
  \begin{itemize}
  \item \textbf{Clinical trial}
    
    \begin{tabular}{cccc}
      $\mu_1$ & $\mu_2$ & $\mu_3$ & $\mu_4$\\
      
      \includegraphics[width=.16\linewidth]{drug1}
      &\includegraphics[width=.16\linewidth]{drug2}
      &\includegraphics[width=.16\linewidth]{drug3}
      &\includegraphics[width=.16\linewidth]{drug4}
    \end{tabular}
    \begin{itemize}
    \item Choose treatment $I_t$ for patient $t$
    \item Observe a response $X_t\in\{0,1\}$ with
      $P(X_t=1)=\mu_{I_t}$. 
    \item Goal: maximize the number of patients healed. 
    \end{itemize}\bigskip\pause
    
  \item \textbf{Online advertisement}, \emph{e.g.}, the choice of a
    title of a news article :
    
    \begin{tabular}{|c|c|}\hline
      Title
      & Click proba. \\\hline
      "Murder victim found in adult entertainment venue"
      & $\mu_1$ \\\hline
      "Headless Body found in Topless Bar"& $\mu_2$\\\hline
    \end{tabular}
    
    \begin{itemize}
    \item Choose which title $I_t$ to display to customer $t$
    \item Observe a response $X_t\in\{0,1\}$ (click or no click).
    \item Goal: maximize your number of clicks. 
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}{Regret minimization}
  We define the regret of a sequence of action
  $\mathcal{I}=(I_1,I_2\dots,I_T)$ as
  \begin{align*}
    \text{Regret}(\mathcal{I})&=\max_{i}\esp{\sum_{t=1}^T R_{t,i}} - \esp{\sum_{t=1}^T R_{t,I_t}}
                           \\
                         &\uncover<2->{=\esp{r_* T - \sum_{t=1}^T
                           R_{t,I_t}},}
  \end{align*}\pause
  where $r_*=\max_i \esp{R_{t,i}}=\max_i \esp{R_{1,i}}$. 
  \bigskip

  Maximizing reward = minimizing regret.
  
  \begin{itemize}
  \item Goal : design strategies that have a small regret for all
    distribution $\mu$. 
  \end{itemize}
\end{frame}

\begin{frame}{Asymptotically optimal regret}
  % \begin{itemize}
  % \item
  A good policy has sub-linear regret:
  \begin{align*}
    \text{Regret}(\mathcal{I}) = o(T). 
  \end{align*}
  % \end{itemize}
  To achieve this, all the arms should be drawn infinitely
  often.\pause 
  
  \begin{theorem}[Lai and Robbins, 1985 (Asymptotically
      Efficient Adaptive Allocation Rules)]
    There exists a constant $c$ (that depend on $\mu$) such that any
    uniformly efficient\footnote{Meaning
      $\text{Regret}(\mathcal{I})=o(T^\alpha)$ for all $\mu$ and $\alpha$.}
    strategy satisfies :
    \begin{align*}
      \text{Regret}(\mathcal{I}) \ge c \log T
    \end{align*}
  \end{theorem}
\end{frame}

\begin{frame}{Some ideas of policies}
  \begin{itemize}
  \item \textbf{Random} -- Draw each arm with probability $1/n$. 
    \begin{itemize}
    \item Exploration
    \end{itemize}\pause\bigskip
  \item \textbf{Greedy:} Always choose the empirical best arm:
    \begin{align*}
      I_{t+1} = \argmax_i \hat{\mu}_i(t)
    \end{align*}
    \begin{itemize}
    \item Exploitation
    \end{itemize}
    \pause\bigskip
  \item \textbf{$\varepsilon$-greedy} : apply ``greedy'' with
    probability $1-\mathbf{\varepsilon}$ and ``random'' otherwise
    (each with probability $\varepsilon/n$)
    \begin{itemize}
    \item Exploration and exploitation. 
    \end{itemize}\pause\bigskip
  \end{itemize}
  \textbf{All have linear regrets}
\end{frame}

\begin{frame}{Analysis of the $\epsilon$-greedy policy ($\epsilon>0$)}
  \begin{itemize}
  \item As $\epsilon>0$, all arm will be chosen an infinite amount of
    time. 
  \item Hence, by the law of large number : $\hat{\mu}_i(t)$ converges
    to the true mean as $t\to\infty$. 
  \item Therefore, the asymptotic regret is 
    \begin{align*}
      \text{Regret}(\text{$\varepsilon$-greedy})=T(\sum_{i}(\mu_*-\mu_i))
      \frac{\varepsilon}{n} + o(T) 
    \end{align*}
  \end{itemize}
\end{frame}

\begin{frame}{$\varepsilon$-greedy : Smaller or larger $\epsilon$ are
    not necessarily better}
  Consider 5 Bernoulli arms with success probabilities
  $\mu=[0.5,0.3,0.6,0.4,0.2]$.  The average reward as a function of
  time is :
  
  \includegraphics[width=.9\linewidth]{epsilon_greedy}
\end{frame}

\section{The UCB Algorithm}
\begin{frame}{UCB builds on Confidence Intervals}
  Consider a coin that gives ``Head'' with probability $\mu$.  Suppose
  that you draw a coin $N$ times and observe $K$ times ``head''. The
  natural estimator of $\mu$ is:
  \begin{align*}
    \hat{\mu} = \frac{K}{N}
  \end{align*}
  \pause
  Hoeffding inequality gives us 
  \begin{align*}
    \ProbaB{\mu \ge \hat{\mu} + \sqrt{\frac{\alpha}{2N}}} \le
    e^{-\alpha}
  \end{align*}
  The idea of USB is to use the above bound with a growing $\alpha$. 
\end{frame}


\begin{frame}{The UCB algorithm}
  UCB computes a confidence bound $UCB_i(t)$ such that
  $\mu_i(t)\le UCB_i(t)$ with high probability.  Example : $UCB1$
  [Auer et al. 02] uses
  \begin{align*}
    UCB_i(t) = \hat{\mu}_i(t) + \sqrt{\frac{\alpha \log t}{2N_i(t)}}.
  \end{align*}
  
  \begin{itemize}
  \item Choose $I_{t+1}\in UCB_i(t)$ (optimism principle). 
  \end{itemize}
  
  \begin{center}
    \pdfmovie{simu/ucb.mp4}{6cm}{4cm}{\includegraphics[width=\linewidth]{ucb_image}}
  \end{center}
\end{frame}

\begin{frame}{Regret of UCB}
  \begin{theorem}
    The algorithm $UCB1$ has a logarithmic regret.  For all
    $\alpha>2$, there exists a constant $C_\alpha>0$ such that if $a$
    is a sub-optimal arm, then $N_i(T)$ (the number of time that this
    arm is chosen before $T$) satisfies
    \begin{align*}
      \esp{N_i(T)} \le \frac{2\alpha\log T}{(\mu_*-\mu_i)^2} +
      C_\alpha. 
    \end{align*}
  \end{theorem}
\end{frame}



\begin{frame}{Analysis of UCB (1/3)}
  \begin{tabular}{cc}
    \mpage{.6}{Using Hoeffding's inequality, one can show
    \begin{eqnarray*}
      \Proba{UCB_i(t)\le\mu_i}&\le& \frac{1}{t^{\alpha-1}}\\
      \Proba{LCB_i(t)\ge\mu_i}&\le& \frac{1}{t^{\alpha-1}},
    \end{eqnarray*}
                                    where
                                    $LCB_i(t) \le \hat{\mu}_i(t) - \sqrt{\frac{\alpha \log
                                    t}{2N_i(t)}}$.
                                    }
                              &\mpage{.4}{\includegraphics[width=\linewidth]{ucb_image}}
  \end{tabular}
\end{frame}

\begin{frame}{Analysis of UCB (2/3)}
  Let us now consider arm $1$ is optimal and arm $2$ is not optimal
  ($\mu_2<\mu_1$). Let $N_2(t)$ be the number of times that arm $2$ is
  chosen between $0$ and $t-1$. Then for any stopping time $\tau$, we
  have:
  \begin{align*}
    &N_2(T)-N_2(\tau) = \sum_{t=\tau}^{T-1} \Ind{I_t=2}\\
    &=\sum_{t=\tau}^{T-1}  \Ind{I_t=2 \land UCB_1(t)\le \mu_1} +
      \Ind{I_t=2 \land UCB_1(t)>
      \mu_1}\\
    &\uncover<2->{=\sum_{t=\tau}^{T-1}  \Ind{I_t=2 \land UCB_1(t)\le \mu_1} +
      \Ind{I_t=2 \land UCB_2(t)>UCB_1(t)> \mu_1}}\\
    &\uncover<3->{\le \sum_{t=\tau}^{T-1}\Ind{UCB_1(t)\le \mu_1}+
      \Ind{UCB_2(t)>\mu_1}}\\
    &\uncover<4->{\le \sum_{t=\tau}^{T-1}\Ind{UCB_1(t)\le \mu_1}
      + \Ind{\mu_2<LCB_2(t)}
      + \Ind{ UCB_2(t)>\mu_1 > \mu_2>LCB_2(t)} }
  \end{align*}
\end{frame}

\begin{frame}{Analysis of UCB (3/3)}
  \begin{align*}
    \sum_{t=\tau}^{T-1}\underbrace{\Ind{UCB_1(t)\le \mu_1}
    + \Ind{\mu_2<LCB_2(t)}}_{\text{occurs with small probability (Hoeffding)}}
    + \underbrace{\Ind{ UCB_2(t)>\mu_1 >
    \mu_2>LCB_2(t)}}_{\text{possible only if $N_2$ is small}} 
  \end{align*}
  Indeed, for the last term:
  \begin{align*}
    \Ind{ LCB_2(t)<\mu_2 <\mu_1<UCB_2(t)}
    &\le\Ind{2\sqrt{\frac{\alpha\log
      t}{2N_2(t)}}>(\mu_1-\mu_2)}=\Ind{N_2(t) \le \frac{2\alpha\log
      T}{(\mu_1-\mu_2)^2}}
  \end{align*}\pause
  
  Let $x=\frac{2\alpha\log T}{(\mu_2-\mu_1)^2}$ and
  $X=\sum_{t=\tau}^{T-1}\Ind{UCB_1(t)\le \mu_1} +
  \Ind{\mu_2<LCB_2(t)}$. We have
  \begin{align*}
    N_2(T)-N_2(\tau) \le  X + \sum_{t=\tau}^T \Ind{N_2(t)\le x}. 
  \end{align*}
  where by Hoeffding's inequality,
  $\expect{X}\le2\sum_{t=1}^{\infty}1/t^{\alpha-1}=:C_\alpha$.\pause 

  By setting $\tau=\inf\{t>0 : N_2(t)\ge x\}$, this implies that:
  \begin{align*}
    \esp{N_2(T)} &\le \esp{N_2(\tau)}+\esp{X} \\
                 &\le C_\alpha + x. 
  \end{align*}
  \qed
\end{frame}


\begin{frame}{Numerical example (same parameter as before)}
  (Cumulative) regret as a function of the number of iterations
  ($\epsilon$-greedy has a linear but small regret)
  \includegraphics[width=.7\linewidth]{regret_comparison_UCB_epsilonGreedy.pdf}
\end{frame}

\section{Thomson Sampling}

\begin{frame}{Bayesian approach : Thomson sampling}
  
  \begin{exampleblock}{Algorithm ``Thomson sampling'' (1933)}
    \begin{itemize}
    \item For each arm $i$, you maintain a probability (posterior)
      distribution $\pi_i^t$ that represent your knowledge on $\mu_i$.
    \item At each time step, you draw $n$ independent samples
      $\theta_i\sim \pi^t_i$. You choose
      $I_{t+1} = \argmax_i \theta_i$.
    \item You update your knowledge $\pi^{t+1}_i$ by using Bayes rule.
    \end{itemize}
  \end{exampleblock}

  \begin{center}
    \pdfmovie{simu/thompson.mp4}{6cm}{4cm}{\includegraphics[width=\linewidth]{thompson_image}}
  \end{center}

\end{frame}

\begin{frame}
  \begin{theorem}[Kauffman,Korda,Munos 2012]
    Thompson Sampling is asymptotically
    optimal\footnote{\url{https://arxiv.org/pdf/1205.4217.pdf }},
    \emph{i.e.} it provides $O(\log T)$ regret with the smallest
    possible constant. 
  \end{theorem}
  It is one of the most efficient algorithm (note : the analysis is
  similar to the one of UCB1). 
\end{frame}

% \begin{frame}{Bayesian update in more detail}
%   Suppose that $X$ is a Bernoulli random variable with a random
%   parameter $\mu$. Assume that you know $\Proba{\mu=\theta}$.
  
%   \begin{itemize}
%   \item If you draw one sample of $X$ and you observe the result, how
%     does your knowledge on $\mu$ evolve?
%   \end{itemize}\pause \bigskip
  
%   The answer comes from Bayes rule : if you observe $X=1$, then : 
%   \begin{align*}
%     \Proba{\mu=\theta \mid X=1} &= \frac{\Proba{X=1 \mid \mu=\theta}
%                                   \Proba{\mu=\theta}}{\Proba{X=1}}\\
%                                 &\propto \theta \Proba{\mu=\theta}
%   \end{align*}
%   Similarly, if you observe $X=0$, then :
%   \begin{align*}
%     \Proba{\mu=\theta \mid X=0} &= \frac{\Proba{X=1 \mid \mu=\theta}
%                                   \Proba{\mu=\theta}}{\Proba{X=1}}\\
%                                 &\propto (1-\theta) \Proba{\mu=\theta}.
%   \end{align*}
% \end{frame}

\begin{frame}{Bayesian update}
  \textbf{Consequence} : Assume now that you start from a uniform
  prior on $\mu$ (\emph{i.e.}  you assume that $\mu$ is uniformly
  distributed on $[0,1]$ and that you draw $n$ samples of $X$.

  Then the distribution of $\mu$ conditioned on having observed $p$
  times the value $1$ is a beta distribution, with probability density
  function $f_{p,n-p}(.)$: 
  \begin{align*}
    f_{p,n-p}(\theta) = c\theta^p(1-\theta)^{1-p},
  \end{align*}
  where $c$ is a constant such that the probability sums to one. \bigskip

  % \begin{exampleblock}{Thompson algorithm rewritten}
  %   \begin{itemize}
  %   \item Let $P_i(t)$ (respectively $N_i(t)$) be the number of time
  %     that arm $i$ has been pulled with a positive (respectively
  %     zero) reward. 
  %   \item Draw $n$ random variables $\theta_1\dots\theta_n$ where
  %     $\theta_i\sim\text{Beta}(P_i(t),N_i(t))$. 
  %   \item Chooses the next arm to be $\argmax_i \theta_i$. 
  %   \end{itemize}
  % \end{exampleblock}
  \bigskip
  
  Note : can be easily adapted to more complicated prior. 
\end{frame}



\begin{frame}{Numerical example (same parameter as before)}
  (Cumulative) regret as a function of the number of iterations
  ($\epsilon$-greedy has a linear but small regret)
  \includegraphics[width=.95\linewidth]{regret_comparison_UCB_TS_epsilonGreedy}
\end{frame}

\section{A Glimpse of Adversarial Bandits}

\begin{frame}{The Adversarial Bandit Model}
  At each time step you choose an action $I_t$ and obtain a reward
  $R_{t,I_t}$.  As before, you want to minimize the expected regret:
  \begin{align*}
    \max_{i\in\{1\dots n\}}\expect{\sum_{t=1}^TR_{t,i}} -
    \expect{\sum_{t=1}^T R_{t,I_t} }
  \end{align*}
  \bigskip
  
  Now, we assume that:
  \begin{itemize}
  \item As before, you do not know $R_{t,I_t}$.
  \item Contrary to before, the distribution of $R_{t,I_t}$ can change
    with time (and can depend on your past choices or rewards).
  \end{itemize}
\end{frame}

\begin{frame}{The Exp3 algorithm}

  Assume that you can observe all $R_{t,i}$ (``expert'' case)
  \begin{itemize}
  \item Compute the accumulated score: $S_{t,i} = \sum_{k=1}^t
    R_{t,i}$
  \item Chooses $I_{t+1}$ randomly such that
    $\Proba{I_{t+1}=i}= \frac{e^{\tau S_{t,i}}}{\sum_{j=1}^ne^{\tau
        S_{t,j}}}$.
  \end{itemize} \bigskip
  
  \textbf{Remarks}:
  \begin{itemize}
  \item This procedures uses a \emph{softmax} operator. \pause
  \item It achieves a reward of $\frac{nT}{2}\tau + \frac{\ln
      n}{\tau}$
    \begin{itemize}
    \item Choosing $\tau=1/\sqrt{T}$ gives you a $O(\sqrt{T})$ regret.  
    \end{itemize}\pause
  \item If you can observe only $R_{t,I_t}$, then in the expression of
    $S_{t,i}$ you can replace $R_{t,i}$ by
    $\hat{R}_{t,i}=R_{t,I_T}\mathbf{1}_{I_t=i}/\Proba{I_{t}=i}$. This
    estimator is unbiased
    \begin{align*}
      \esp{\hat{R}_{t,i}} = R_{t,i}. 
    \end{align*}
    It also gives a $O(1/\sqrt{T})$ regret. 
  \end{itemize}

\end{frame}

\begin{frame}{Numerical example (same parameter as before)}
  {We look at a stochastic reward}
  (Cumulative) regret as a function of the number of iterations
  \includegraphics[width=.95\linewidth]{regret_comparison_UCB_TS_epsilonGreedy_exp3}
\end{frame}

\section{Conclusion}


\begin{frame}{What did we learn?}
  \begin{itemize}
  \item A way to quantify the tradeoff between exploration and
    exploitation.
  \item An algorithm that achieve this optimal tradeoff (Thompson
    sampling) and that uses a Bayesian framework.  
  \end{itemize}\bigskip 

  % To go further : 
  % \begin{itemize}
  % \item Contextual bandits
  % \item Adversarial bandits 
  % \item Markov bandits
  % \end{itemize}

  %  Multi-Armed bandit is a general framework with many variants
  % \begin{itemize}
  % \item Good example of exploration--exploitation tradeoff
  % \item Probabilistic model (Bayesian) or model-free
  % \item Many variants not covered : Adversarial, contextual,
  %   time-varying,...
  % \end{itemize}
  % \bigskip

  Multi-armed bandits have many variants and applications
  \begin{itemize}
  \item Online problems
  \item Reinforcement learning
  \end{itemize}
\end{frame}


\begin{frame}{Going further}
  \begin{itemize}
  \item Markovian bandit problem
    \begin{itemize}
    \item Index policies (Gittins and Whittle) (see Kimang and Chen)
    \end{itemize}
  \item Combinatorial bandits
    \begin{itemize}
    \item UCB or Exp3-like algorithm. (see Quan for application to
      routing) 
    \end{itemize}
  \item Continuous actions
    \begin{itemize}
    \item Online convex optimization (see Panayotis)
    \end{itemize}
  \item Mixing bandit problem and Markov decision processes 
    \begin{itemize}
    \item Reinforcement learning (See Bruno's talk)
    \end{itemize}
  \end{itemize}

  
  \bigskip
  Some references: 
  
  \centering 
  \begin{tabular}{ccc}
    \includegraphics[width=.3\linewidth]{book_suttonBarto}
    &\includegraphics[width=.2\linewidth]{bandit_BCB}\\
    Stochastic bandits&Stochastic \& Adversarial
    (Chapter 1)
  \end{tabular}
  \bigskip
  
  + ask Panayotis if you have questions.
  
\end{frame}



\end{document}